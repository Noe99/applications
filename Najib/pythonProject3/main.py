###########################################################
### Nom ......... : inconu
### Role ........ : inconu

### Auteur ...... : Noe Berguin
### Version ..... : V1.1 du 2021-07-07
### Licence ..... : Polytechnique Montreal Najib Bouaanani
###########################################################
import math

import xlwt

from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QMessageBox, QMenu
from importDialog import Ui_Dialog



class Ui_MainWindow(QtWidgets.QMainWindow):
    pen = pg.mkPen(color=(255, 0, 0), width=3)  ## color use to draw
    periods =[]
    sa =[]
    screenWidth = 0
    screenHeight = 0

    def sizePourcentage(self, width, height):
        return QtCore.QSize(int(self.screenWidth * (width/100)), int(self.screenHeight * (height/100)))

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.showMaximized()
        screen = app.primaryScreen().size()
        self.screenWidth = screen.width()
        self.screenHeight = screen.height()

        graphicsViewSize = self.sizePourcentage(75, 60)
        tableSize = self.sizePourcentage(15, 30)
        tableSize2 = self.sizePourcentage(75, 24.9)
        iconSize = self.sizePourcentage(1, 2)


        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setAutoFillBackground(True)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setMaximumSize(QtCore.QSize(16777215, 20))
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.line = QtWidgets.QFrame(self.centralwidget)
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.verticalLayout.addWidget(self.line)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.radioButton_2 = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton_2.setObjectName("radioButton_2")
        self.horizontalLayout_2.addWidget(self.radioButton_2)
        self.radioButton = QtWidgets.QRadioButton(self.centralwidget)
        self.radioButton.setObjectName("radioButton")
        self.horizontalLayout_2.addWidget(self.radioButton)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setMaximumSize(QtCore.QSize(368, 15))
        self.label_2.setObjectName("label_2")
        self.verticalLayout.addWidget(self.label_2)
        self.line_5 = QtWidgets.QFrame(self.centralwidget)
        self.line_5.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_5.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_5.setObjectName("line_5")
        self.verticalLayout.addWidget(self.line_5)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout_3.addWidget(self.label_3)
        self.comboBox = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")
        self.horizontalLayout_3.addWidget(self.comboBox)
        self.verticalLayout.addLayout(self.horizontalLayout_3)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem1)
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout.addWidget(self.pushButton)
        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.verticalLayout.addWidget(self.pushButton_2)
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setMaximumSize(tableSize)
        self.tableWidget.setMinimumSize(tableSize)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(2)
        self.tableWidget.setRowCount(10)
        item = QtWidgets.QTableWidgetItem()
        item.setBackground(QtGui.QColor(0, 0, 0))
        self.tableWidget.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget.setHorizontalHeaderItem(1, item)

        index = 0
        while index < 10:
            self.tableWidget.setItem(index, 0, QtWidgets.QTableWidgetItem(str('')))
            self.tableWidget.setItem(index, 1, QtWidgets.QTableWidgetItem(str('')))
            index += 1

        self.tableWidget.setColumnWidth(0, int(tableSize.width()/2.3))
        self.tableWidget.setColumnWidth(1, int(tableSize.width() / 2.4))
        self.verticalLayout.addWidget(self.tableWidget)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout()
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_4.addWidget(self.label_4)
        self.spinBox = QtWidgets.QSpinBox(self.centralwidget)
        self.spinBox.setObjectName("spinBox")
        self.horizontalLayout_4.addWidget(self.spinBox)
        self.verticalLayout_5.addLayout(self.horizontalLayout_4)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout_5.addWidget(self.label_5)
        self.comboBox_2 = QtWidgets.QComboBox(self.centralwidget)
        self.comboBox_2.setObjectName("comboBox_2")
        self.comboBox_2.addItem("")
        self.horizontalLayout_5.addWidget(self.comboBox_2)
        self.verticalLayout_5.addLayout(self.horizontalLayout_5)
        self.verticalLayout.addLayout(self.verticalLayout_5)
        spacerItem2 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem2)
        self.verticalLayout_7 = QtWidgets.QVBoxLayout()
        self.verticalLayout_7.setObjectName("verticalLayout_7")
        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setMaximumSize(QtCore.QSize(366, 13))
        self.label_6.setObjectName("label_6")
        self.verticalLayout_7.addWidget(self.label_6)
        self.line_2 = QtWidgets.QFrame(self.centralwidget)
        self.line_2.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_2.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_2.setObjectName("line_2")
        self.verticalLayout_7.addWidget(self.line_2)
        self.horizontalLayout_7 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_7.setObjectName("horizontalLayout_7")
        self.label_7 = QtWidgets.QLabel(self.centralwidget)
        self.label_7.setMaximumSize(QtCore.QSize(16, 20))
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_7.addWidget(self.label_7)
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout_7.addWidget(self.lineEdit)
        self.verticalLayout_7.addLayout(self.horizontalLayout_7)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_8 = QtWidgets.QLabel(self.centralwidget)
        self.label_8.setMaximumSize(QtCore.QSize(20, 20))
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_6.addWidget(self.label_8)
        self.lineEdit_2 = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.horizontalLayout_6.addWidget(self.lineEdit_2)
        self.verticalLayout_7.addLayout(self.horizontalLayout_6)
        self.verticalLayout.addLayout(self.verticalLayout_7)
        spacerItem3 = QtWidgets.QSpacerItem(20, 15, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem3)
        self.verticalLayout_8 = QtWidgets.QVBoxLayout()
        self.verticalLayout_8.setObjectName("verticalLayout_8")
        self.label_9 = QtWidgets.QLabel(self.centralwidget)
        self.label_9.setMaximumSize(QtCore.QSize(366, 13))
        self.label_9.setObjectName("label_9")
        self.verticalLayout_8.addWidget(self.label_9)
        self.line_3 = QtWidgets.QFrame(self.centralwidget)
        self.line_3.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_3.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_3.setObjectName("line_3")
        self.verticalLayout_8.addWidget(self.line_3)
        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.verticalLayout_8.addWidget(self.pushButton_3)
        self.verticalLayout.addLayout(self.verticalLayout_8)
        spacerItem4 = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem4)
        self.verticalLayout_9 = QtWidgets.QVBoxLayout()
        self.verticalLayout_9.setObjectName("verticalLayout_9")
        self.label_10 = QtWidgets.QLabel(self.centralwidget)
        self.label_10.setMaximumSize(QtCore.QSize(366, 13))
        self.label_10.setObjectName("label_10")
        self.verticalLayout_9.addWidget(self.label_10)
        self.line_4 = QtWidgets.QFrame(self.centralwidget)
        self.line_4.setFrameShape(QtWidgets.QFrame.HLine)
        self.line_4.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_4.setObjectName("line_4")
        self.verticalLayout_9.addWidget(self.line_4)
        self.horizontalLayout_10 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_10.setObjectName("horizontalLayout_10")
        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.horizontalLayout_10.addWidget(self.pushButton_4)
        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setObjectName("pushButton_5")
        self.horizontalLayout_10.addWidget(self.pushButton_5)
        self.verticalLayout_9.addLayout(self.horizontalLayout_10)
        self.verticalLayout.addLayout(self.verticalLayout_9)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.horizontalLayout_8 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_8.setObjectName("horizontalLayout_8")
        self.toolButton_3 = QtWidgets.QToolButton(self.centralwidget)
        self.toolButton_3.setText("")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("./buttons/zoom-in.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton_3.setIcon(icon)
        self.toolButton_3.setIconSize(iconSize)
        self.toolButton_3.setObjectName("toolButton_3")
        self.horizontalLayout_8.addWidget(self.toolButton_3)
        spacerItem5 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem5)
        self.toolButton_2 = QtWidgets.QToolButton(self.centralwidget)
        self.toolButton_2.setText("")

        ## Create icon for zoom button
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("./buttons/magnifying-glass.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)


        self.toolButton_2.setIcon(icon1)
        self.toolButton_2.setIconSize(iconSize)
        self.toolButton_2.setObjectName("toolButton_2")
        self.horizontalLayout_8.addWidget(self.toolButton_2)
        spacerItem6 = QtWidgets.QSpacerItem(20, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem6)
        self.toolButton = QtWidgets.QToolButton(self.centralwidget)
        self.toolButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("./buttons/padlock.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton.setIcon(icon2)
        self.toolButton.setIconSize(iconSize)
        self.toolButton.setObjectName("toolButton")
        self.horizontalLayout_8.addWidget(self.toolButton)
        spacerItem7 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_8.addItem(spacerItem7)
        self.verticalLayout_4.addLayout(self.horizontalLayout_8)
        self.graphicsView = pg.PlotWidget(self.centralwidget)
        self.graphicsView.setBackground('w')
        styles = {'color': 'w', 'font-size': '%spx' % (str(int(20)))}
        self.graphicsView.setLabel('left', 'Abs', **styles)
        self.graphicsView.setLabel('bottom', 'Ord', **styles)
        self.graphicsView.showGrid(x=True, y=True)
        self.graphicsView.setMinimumSize(graphicsViewSize)
        self.graphicsView.setObjectName("graphicsView")
        self.verticalLayout_4.addWidget(self.graphicsView)
        self.horizontalLayout_9 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_9.setObjectName("horizontalLayout_9")
        self.pushButton_7 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_7.setObjectName("pushButton_7")
        self.horizontalLayout_9.addWidget(self.pushButton_7)
        self.pushButton_8 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_8.setObjectName("pushButton_8")
        self.horizontalLayout_9.addWidget(self.pushButton_8)
        self.pushButton_9 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_9.setObjectName("pushButton_9")
        self.horizontalLayout_9.addWidget(self.pushButton_9)
        self.pushButton_6 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_6.setObjectName("pushButton_6")
        self.horizontalLayout_9.addWidget(self.pushButton_6)
        self.verticalLayout_4.addLayout(self.horizontalLayout_9)
        self.tableWidget_2 = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget_2.setMinimumSize(tableSize2)
        self.tableWidget_2.setMaximumSize(QtCore.QSize(16777215, 250))
        self.tableWidget_2.setObjectName("tableWidget_2")
        self.tableWidget_2.setColumnCount(4)
        self.tableWidget_2.setRowCount(0)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(0, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(1, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(2, item)
        item = QtWidgets.QTableWidgetItem()
        self.tableWidget_2.setHorizontalHeaderItem(3, item)
        self.tableWidget_2.setColumnWidth(0, int(tableSize2.width() / 4.1))
        self.tableWidget_2.setColumnWidth(1, int(tableSize2.width() / 4))
        self.tableWidget_2.setColumnWidth(2, int(tableSize2.width() / 4))
        self.tableWidget_2.setColumnWidth(3, int(tableSize2.width() / 4.1))

        self.verticalLayout_4.addWidget(self.tableWidget_2)
        self.horizontalLayout.addLayout(self.verticalLayout_4)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        spacerItem8 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem8)
        self.groupBox = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox.setMinimumSize(QtCore.QSize(150, 150))
        self.groupBox.setMaximumSize(QtCore.QSize(150, 150))
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_3.addWidget(self.groupBox)
        spacerItem9 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout_3.addItem(spacerItem9)
        self.toolButton_4 = QtWidgets.QToolButton(self.centralwidget)
        self.toolButton_4.setEnabled(True)
        self.toolButton_4.setText("")
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("./buttons/undo.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.toolButton_4.setIcon(icon3)
        self.toolButton_4.setIconSize(iconSize)
        self.toolButton_4.setObjectName("toolButton_4")
        self.verticalLayout_3.addWidget(self.toolButton_4)
        spacerItem10 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_3.addItem(spacerItem10)
        self.horizontalLayout.addLayout(self.verticalLayout_3)

        self.verticalLayout_2.addLayout(self.horizontalLayout)
        MainWindow.setCentralWidget(self.centralwidget)

        self.proxy1 = pg.SignalProxy(self.graphicsView.scene().sigMouseMoved, rateLimit=60, ## Mouse mouve signal connection
                                     slot=self.mouseMovedAcc)

        self.cursorAcc = self.graphicsView.plot([0], [0], pen=self.pen, symbol='o', symbolSize=15, ## Mouse cursor in the graphicsView
                                                symbolBrush=(125, 125, 125),
                                                width=3)
        self.textAcc = pg.TextItem(
            str(str(format(0, '.4f')) + ' (s)\n' + str(format(0, '.4f')) + ' (m/s2)'))  ## text in the graphicsView
        self.graphicsView.addItem(self.textAcc, ignoreBounds=True)
        self.textAcc.setPos(0, 0)
        self.textAcc.setColor(color=(0, 0, 0))

        self.vbAcc = self.graphicsView.getViewBox()
        self.vbAcc.sigXRangeChanged.connect(self.setYRangeAcc)


        self.connectButtons()
        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


    def setYRangeAcc(self):
        self.vbAcc.enableAutoRange(axis='y')
        self.vbAcc.setAutoVisible(y=True)


    #########################################################
    # retranslateUi : appropriate the correct name
    # of all the graphics object of the window
    #########################################################
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Type of scalling"))
        self.radioButton_2.setText(_translate("MainWindow", "Single component"))
        self.radioButton.setText(_translate("MainWindow", "Multi-Component"))
        self.label_2.setText(_translate("MainWindow", "Menu"))
        self.label_3.setText(_translate("MainWindow", "Scalling method"))
        self.comboBox.setItemText(0, _translate("MainWindow", "Partial Spectral Area"))
        self.comboBox.setItemText(1, _translate("MainWindow", "autre"))
        self.pushButton.setText(_translate("MainWindow", "Define Vibration Periods"))
        self.pushButton_2.setText(_translate("MainWindow", "Define target Spectrum"))
        item = self.tableWidget.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Periods (s)"))
        item = self.tableWidget.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Sa (g)"))
        self.label_4.setText(_translate("MainWindow", "Number of Groud Motions"))
        self.label_5.setText(_translate("MainWindow", "Scale Factor Limit"))
        self.comboBox_2.setItemText(0, _translate("MainWindow", "Default"))
        self.label_6.setText(_translate("MainWindow", "Scale Factor Limit"))
        self.label_7.setText(_translate("MainWindow", "min"))
        self.label_8.setText(_translate("MainWindow", "max"))
        self.label_9.setText(_translate("MainWindow", "Upload User Data"))
        self.pushButton_3.setText(_translate("MainWindow", "Upload Data"))
        self.label_10.setText(_translate("MainWindow", "Run"))
        self.pushButton_4.setText(_translate("MainWindow", "Run"))
        self.pushButton_5.setText(_translate("MainWindow", "Load"))
        self.pushButton_7.setText(_translate("MainWindow", "Target Spectrum"))
        self.pushButton_8.setText(_translate("MainWindow", "Selected Ground Motions"))
        self.pushButton_9.setText(_translate("MainWindow", "Scaled Groud Motion"))
        self.pushButton_6.setText(_translate("MainWindow", "Final Scaling"))
        item = self.tableWidget_2.horizontalHeaderItem(0)
        item.setText(_translate("MainWindow", "Earthquake Component Label"))
        item = self.tableWidget_2.horizontalHeaderItem(1)
        item.setText(_translate("MainWindow", "Station"))
        item = self.tableWidget_2.horizontalHeaderItem(2)
        item.setText(_translate("MainWindow", "Scaling factor"))
        item = self.tableWidget_2.horizontalHeaderItem(3)
        item.setText(_translate("MainWindow", "Standar deviation"))
        self.groupBox.setTitle(_translate("MainWindow", "LEGEND"))

    def eventFilter(self, source, event):
        if (event.type() == QtCore.QEvent.MouseButtonPress and
                event.buttons() == QtCore.Qt.RightButton and
                source is self.tableWidget.viewport()):
            item = self.tableWidget.itemAt(event.pos())
            if item is not None:
                self.menu = QMenu(self)
                copie = self.menu.addAction('Paste')  # (QAction('test'))
                action = self.menu.exec_(event.globalPos())
                if action == copie:
                    self.pasteAction()
        return super(Ui_MainWindow, self).eventFilter(source, event)

    def extractData(self, data):
        i=0
        while i< len(data):
            self.periods.append(data[i])
            self.sa.append(data[i+1])
            i+=2

    def changeStringToFloat(self, test_list):
        for i in range(0, len(test_list)):
            test_list[i] = float(test_list[i])

    def pasteAction(self):
        print('paste')
        print(QtWidgets.qApp.clipboard().text())
        list = QtWidgets.qApp.clipboard().text().split()
        self.changeStringToFloat(list)
        self.extractData(list)
        print(self.periods)
        print(self.sa)
        self.showData()

    #########################################################
    # connectButtons : Connect signal and slots together
    #########################################################
    def connectButtons(self):
        self.pushButton_2.clicked.connect(self.importData)
        self.toolButton_4.clicked.connect(lambda: self.graphicsView.getPlotItem().enableAutoRange())
        self.toolButton_2.clicked.connect(self.pasteAction)
        self.tableWidget.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.tableWidget.viewport().installEventFilter(self)


    #########################################################
    # findClosePoint : Algorithm to find the closest point between
    # the X position of your mouse and the list of periods
    #########################################################
    def findClosePoint(self, x, listX):
        i = 1;
        temp = math.fabs(listX[0] - x)
        exit = 0
        while i < len(listX):
            dx = math.fabs(listX[i] - x)
            if dx < temp:
                temp = dx
                exit = i
            else:
                i =  len(listX)
            i += 1
        return exit

    #########################################################
    # mouseMovedAcc : Define the mousse mouve slot.
    # it's the action to do each time the signal 'MousMouve' appear.
    #########################################################
    def mouseMovedAcc(self, e):
        pos = e[0]
        positionX = self.graphicsView.plotItem.vb.mapSceneToView(pos).x()
        if len(self.periods) > 0 and len(self.sa) > 0:
            point = self.findClosePoint(positionX, self.periods)
            self.showCursorAcc(point, self.periods, self.sa)


    #########################################################
    # showCursorAcc : Remove the past cursor of the graphicView and
    # create a new one next to the mouse position
    #########################################################
    def showCursorAcc(self, point, listX, listY):
        x = listX[point]
        y = listY[point]
        self.graphicsView.removeItem(self.cursorAcc)
        self.graphicsView.removeItem(self.textAcc)
        self.cursorAcc = self.graphicsView.plot([x], [y], pen=self.pen, symbol='o', symbolSize=15, symbolBrush=(125, 125, 125), width=3)
        self.textAcc = pg.TextItem(
            str(str(format(x, '.4f')) + ' (s)\n' + str(format(y, '.4f')) + ' (m/s2)'))
        self.graphicsView.addItem(self.textAcc, ignoreBounds=True)
        self.textAcc.setPos(x, y)
        self.textAcc.setColor(color=(0, 0, 0))
        self.label_7.setText(str(str(format(x, '.4f')) + ' (m/s2)'))
        self.label_6.setText(str(str(format(y, '.4f')) + ' (s)'))

    def getMax(self, list):
        index=0
        max = list[0]
        while index < len(list):
            if max < list[index]:
                max = list[index]
            index+=1
        return max + 1

    def getMin(self, list):
        index=0
        min = list[0]
        while index < len(list):
            if min > list[index]:
                min = list[index]
            index+=1
        return min - 1

    #########################################################
    # importData : Import data from an extern file, Excel or text.
    #########################################################
    def importData(self):
        print('connect')
        dialog = Ui_Dialog()
        if len(dialog.periods) > 0:
            self.periods = dialog.periods
            self.sa = dialog.sa
            self.showData()


    def showData(self):
        self.pen = pg.mkPen(color=(0, 0, 255), width=3)
        self.graphicsView.plot(self.periods, self.sa, pen=self.pen)
        self.graphicsView.getViewBox().setLimits(xMin=self.periods[0] - 1,
                                                 xMax=self.periods[len(self.periods) - 1] + 1,
                                                 yMin=self.getMin(self.sa),
                                                 yMax=self.getMax(self.sa))

        self.tableWidget.setRowCount(len(self.periods))
        index = 0
        while index < len(self.periods):
            self.tableWidget.setItem(index, 0, QtWidgets.QTableWidgetItem(str(self.periods[index])))
            self.tableWidget.setItem(index, 1, QtWidgets.QTableWidgetItem(str(self.sa[index])))
            index += 1



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
